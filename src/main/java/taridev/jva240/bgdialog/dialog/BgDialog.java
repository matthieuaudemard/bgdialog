package taridev.jva240.bgdialog.dialog;

import taridev.jva240.bgdialog.composite.BoardComposite;
import taridev.jva240.bgdialog.composite.OptionComposite;
import taridev.jva240.bgdialog.vo.BoardVo;
import taridev.tdevvu.exception.AbstractBusinessException;

import javax.swing.*;
import java.awt.*;

public class BgDialog extends JFrame {

    private BgDialog(BoardVo vo) throws AbstractBusinessException {
        OptionComposite optionComposite = new OptionComposite(vo.getOptionVo());
        BoardComposite boardComposite = new BoardComposite(vo);

        setSize(600, 300);
        setLayout(new BorderLayout());
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        getContentPane().add(optionComposite, BorderLayout.PAGE_START);
        getContentPane().add(boardComposite, BorderLayout.CENTER);
    }

    public static void main(String[] args) throws AbstractBusinessException {
        new BgDialog(new BoardVo()).setVisible(true);
    }

}
