package taridev.jva240.bgdialog.composite;

import taridev.tdevvu.composite.AbstractCompositeWithHelper;
import taridev.tdevvu.exception.AbstractBusinessException;
import taridev.tdevvu.factory.GraphicFactory;
import taridev.jva240.bgdialog.vo.OptionVo;

import javax.swing.*;
import java.awt.*;

public class OptionComposite extends AbstractCompositeWithHelper<OptionVo, OptionCompositeHelper> {

    private JSlider slider = null;
    private JButton color1 = null;
    private JButton color2 = null;

    public OptionComposite(OptionVo model) throws AbstractBusinessException {
        super(model);
    }

    @Override
    public void innerCreateContent() {
        OptionVo vo = getVo();

        GraphicFactory.createLabel(this, vo, "interval");

        slider = GraphicFactory.createSlider(this, vo, "interval");
        color1 = GraphicFactory.createButton(this, "Couleur 1");
        color2 = GraphicFactory.createButton(this, "Couleur 2");

        color1.setOpaque(true);
        color1.setBackground(vo.getColor1());
        color1.setBorderPainted(false);
        color2.setOpaque(true);
        color2.setBackground(vo.getColor2());
        color2.setBorderPainted(false);
    }

    @Override
    public void mapComponentsToListeners() {
        slider.addChangeListener(e -> getParent().repaint());
        color1.addActionListener(e -> {
            Color color = JColorChooser.showDialog(this, "Choisir une couleur", getVo().getColor1());
            if (color != null) {
                getVo().setColor1(color);
                ((JButton) e.getSource()).setBackground(color);
                getParent().repaint();
            }
        });
        color2.addActionListener(e -> {
            Color color = JColorChooser.showDialog(this, "Choisir une couleur", getVo().getColor2());
            if (color != null) {
                getVo().setColor2(color);
                ((JButton) e.getSource()).setBackground(color);
                getParent().repaint();
            }
        });
    }
}
