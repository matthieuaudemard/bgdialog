package taridev.jva240.bgdialog.composite;

import taridev.tdevvu.composite.AbstractCompositeHelper;
import taridev.jva240.bgdialog.vo.BoardVo;
import taridev.jva240.bgdialog.vo.OptionVo;

import java.awt.*;

public class BoardCompositeHelper extends AbstractCompositeHelper<BoardComposite> {

    public BoardCompositeHelper(BoardComposite composite) {
        super(composite);
    }

    void paint(Graphics g) {
        final BoardComposite composite = getComposite();
        final BoardVo castedVo = (BoardVo) getVO();
        final OptionVo optionVo = castedVo.getOptionVo();
        final int width = composite.getWidth();
        final int height = composite.getHeight();
        final int interval = optionVo.getInterval();

        // Coloration de l'arrière plan
        g.setColor(castedVo.getBgColor());
        g.fillRect(0, 0, width, height);

        // Dessin des lignes
        for (int i = 0 ; i < interval ; i++) {
            g.setColor(optionVo.getColor1());
            g.drawLine(0, height - (height * i / interval), width * i / interval, 0);
            g.setColor(optionVo.getColor2());
            g.drawLine(width - (width * i / interval), height, width, height * i / interval);
        }
    }
}
