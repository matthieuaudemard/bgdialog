package taridev.jva240.bgdialog.composite;

import taridev.tdevvu.composite.AbstractCompositeHelper;

public class OptionCompositeHelper extends AbstractCompositeHelper<OptionComposite> {

    public OptionCompositeHelper(OptionComposite composite) {
        super(composite);
    }
}
