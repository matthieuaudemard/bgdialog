package taridev.jva240.bgdialog.composite;

import taridev.jva240.bgdialog.vo.BoardVo;
import taridev.tdevvu.composite.AbstractCompositeWithHelper;
import taridev.tdevvu.exception.AbstractBusinessException;

import java.awt.*;

public class BoardComposite extends AbstractCompositeWithHelper<BoardVo, BoardCompositeHelper> {

    public BoardComposite(BoardVo model) throws AbstractBusinessException {
        super(model);
    }

    @Override
    protected void paintComponent(Graphics g) {
        getHelper().paint(g);
    }

    @Override
    public void innerCreateContent() {
        // Pas de composant à créer ici ...
    }

    @Override
    public void mapComponentsToListeners() {
        // Pas de comportement à créer ici ...
    }
}
