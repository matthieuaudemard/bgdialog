package taridev.jva240.bgdialog.vo;

import taridev.tdevvu.vo.AbstractVo;

import java.awt.*;

public class OptionVo extends AbstractVo {

    private Integer interval = 12;
    private Color color1 = Color.BLUE;
    private Color color2 = Color.RED;

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        if (interval.compareTo(this.interval) != 0) {
            changeSupport.firePropertyChange("interval", this.interval, interval);
            this.interval = interval;
        }
    }

    public Color getColor1() {
        return color1;
    }

    public void setColor1(Color color1) {
        changeSupport.firePropertyChange("color1", this.color1, color1);
        this.color1 = color1;
    }

    public Color getColor2() {
        return color2;
    }

    public void setColor2(Color color2) {
        changeSupport.firePropertyChange("color2", this.color2, color2);
        this.color2 = color2;
    }
}
