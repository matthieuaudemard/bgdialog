package taridev.jva240.bgdialog.vo;


import taridev.tdevvu.vo.AbstractVo;

import java.awt.*;

public class BoardVo extends AbstractVo {

    private Color bgColor = Color.WHITE;
    private OptionVo optionVo = new OptionVo();

    public Color getBgColor() {
        return bgColor;
    }

    public void setBgColor(Color bgColor) {
        changeSupport.firePropertyChange("color2", this.bgColor, bgColor);
        this.bgColor = bgColor;
    }

    public OptionVo getOptionVo() {
        return optionVo;
    }

    public void setOptionVo(OptionVo optionVo) {
        changeSupport.firePropertyChange("options", this.optionVo, optionVo);
        this.optionVo = optionVo;
    }
}
